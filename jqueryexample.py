# -*- coding: utf-8 -*-
"""
    jQuery Example
    ~~~~~~~~~~~~~~

    A simple application that shows how Flask and jQuery get along.

    :copyright: (c) 2015 by Armin Ronacher.
    :license: BSD, see LICENSE for more details.
"""
from flask import Flask, jsonify, render_template, request, session, abort
import random
import string
import os
import socket
app = Flask(__name__)


@app.route('/_add_numbers')
def add_numbers():
    """Add two numbers server side, ridiculous but well..."""
    a = request.args.get('a', 0, type=int)
    b = request.args.get('b', 0, type=int)
    return jsonify(result=a + b)

@app.route('/form')
def form():
    return render_template('form.html')

@app.route('/')
def index():
    return render_template('index.html')

#@app.route("/ping", methods=['GET', 'POST'])
@app.route("/ping")
#@app.route("/ping/<hostname>")
def check():
	if request.args.get("hostname") == "" and request.args.get("port") == "":
		#print("Please fill up form")
		return jsonify(result="Please fill up form")
	elif request.args.get("hostname") != "" and request.args.get("port") == "":
		response = os.system("ping -W 1 -c 1 "+ request.args.get("hostname") +"> /dev/null 2>&1")
		if response == 0:
			return jsonify(result="%s is up!" % request.args.get("hostname"))
		else:
			return jsonify(result="%s is down!" % request.args.get("hostname"))
	elif request.args.get("hostname") != "" and request.args.get("port") != "":
		s = socket.socket()
		s.settimeout(2)
		try:
			s.connect((request.args.get("hostname"), int(request.args.get("port"))))
			return jsonify(result="Connected to %s on port %s" % (request.args.get("hostname"), request.args.get("port")))
		except socket.error:
			return jsonify(result="Connected to %s on port %s failed" % (request.args.get("hostname"), request.args.get("port")))
	else:
		return jsonify(result="Something is wrong")

@app.before_request
def csrf_protect():
    if request.method == "POST":
        token = session.pop('_csrf_token', None)
        if not token or token != request.args.get('_csrf_token'):
            abort(403)

def generate_csrf_token():
    if '_csrf_token' not in session:
        session['_csrf_token'] = some_random_string()
    return session['_csrf_token']

app.jinja_env.globals['csrf_token'] = generate_csrf_token
app.secret_key = 'A0Zr98j/3yX R~XHH!jmN]LWX/,?RT'


def some_random_string():
    return ''.join(random.choice(string.ascii_letters) for x in range(13))


if __name__ == '__main__':
    app.run(host='0.0.0.0')
